* Creating fully static builds for linux

```
CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' src/echo_server.go
```
