package main
import "fmt"

func evenStream() func() uint {
    i := uint(0)
    return func() (ret uint) {
        ret = i
        i += 2
        return
    }
}

func oddStream() func() uint {
    i := uint(1)
    return func() uint {
        ret := i
        i += 2
        return ret
    }
}

func main() {
    nextEven := evenStream()
    fmt.Println(nextEven())
    fmt.Println(nextEven())
    fmt.Println(nextEven())
    fmt.Println(nextEven())

    nextOdd := oddStream()
    fmt.Println(nextOdd())
    fmt.Println(nextOdd())
    fmt.Println(nextOdd())
    fmt.Println(nextOdd())
}
