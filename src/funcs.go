package main
import "fmt"

func avg(xs []float64) float64 {
    total := 0.0
    for _, v := range xs {
        total += v
    }
    return total / float64(len(xs))
}

func add(args ...int) int {
    total := 0
    for _, v := range args {
        total += v
    }
    return total
}

func main() {
    xs := []float64{98, 93, 77, 82, 83 }
    avgVal := avg(xs)
    fmt.Println(avgVal)

    fmt.Println(add(1,2,3))
    fmt.Println(add())
    ys := []int{2,4,6}
    fmt.Println(add(ys...))
}
