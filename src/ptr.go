package main

import "fmt"

func swap(x *int, y *int) {
    *x, *y = *y, *x
}

func main() {
    a := 1
    b := 2
    fmt.Println("a=", a, ",b=", b)
    swap(&a, &b)
    fmt.Println("a=", a, ",b=", b)
}
