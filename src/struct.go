package main
import ("fmt"; "math")

func distance (x1, y1, x2, y2 float64) float64 {
    a := x2 - x1
    b := y2 - y1
    return math.Sqrt(a*a + b*b)
}

type Point struct {
    x, y float64
}

type Rectangle struct {
    bottomLeft, topRight *Point
}

func (r *Rectangle) area() float64 {
    l := distance(r.bottomLeft.x, r.bottomLeft.y, r.bottomLeft.x, r.topRight.y)
    w := distance(r.bottomLeft.x, r.bottomLeft.y, r.topRight.x, r.bottomLeft.y)
    return l * w
}

type Circle struct {
    p *Point
    r float64
}

func circleArea(c Circle) float64 {
    return math.Pi * c.r * c.r
}

func (c *Circle) area() float64 {
    return math.Pi * c.r * c.r
}

func main() {
  c := Circle{p:&Point{x: 0, y: 0}, r:5}
  fmt.Println(circleArea(c))
  fmt.Println(c.area())

  r := Rectangle{bottomLeft:&Point{x:10, y:10}, topRight:&Point{x:25, y:25}}
  fmt.Println(r.area())
}
